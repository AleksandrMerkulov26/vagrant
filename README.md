# vagrant

## Установка
- [Файлы для установки](https://drive.google.com/drive/u/0/folders/1Ev8N8LijxNR2npEwhoUFlxBuznf--ujP) для Win и Mac
- [Пошаговая инструкция по установке](https://docs.google.com/document/d/1g7dFTNLvBHC3zyS4cKpTjr0geRCdT_NfL8Ja42GlNAM/edit)
- [Импорт образа с Ubuntu](https://docs.google.com/document/d/1hMS1sZ7YtsmP1TKq5RDVCOLM4a0UGq2_kZVt7yUx-bs/edit)
- Установка [VirtualBox, Vagrant+Ubuntu и Vagrantfile](https://www.youtube.com/watch?v=dgm5MtCcIMs&t=5150s)

## Написание Vagrantfile
- [Vagrant Provisioning](https://www.vagrantup.com/docs/provisioning) и [на русском](https://automation-remarks.com/setting-vagrant/)
- [Полная документация по Vagrant для самоподготовки](https://www.vagrantup.com/docs)

## Интерактивное обучение
- Начало работы: https://learn.hashicorp.com/vagrant
- Сети и Провижн: https://learn.hashicorp.com/collections/vagrant/networking-provisioning-operations
